
const routes = [
  {
    path: '/',
    component: () => import('layouts/login.vue'),
    children: [
      { path: '', component: () => import('pages/login.vue') }
    ]
  },
  {
    path: '/index',
    // component: () => import('layouts/index.vue'),
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue') }
    ]
  },
  {
    path: '/register',
    component: () => import('layouts/register.vue'),
    children: [
      { path: '', component: () => import('pages/register.vue') }
    ]
  },
  // {
  //   path: '/teste2',
  //   component: () => import('layouts/teste.vue'),
  //   children: [
  //     { path: '', component: () => import('pages/page2.vue') }
  //   ]
  // }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
