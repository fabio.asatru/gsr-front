// import state from './state'
import {getters} from './getters'
import {mutations} from './mutations'
import {actions} from './actions'

const state = {
  login : {
    username: '',
    email: '',
    token: localStorage.getItem('token'),
    refreshToken: null,
    userId: 0
  }
}

const namespaced = true

export const login = {
  namespaced,
  state,
  getters,
  mutations,
  actions
}
