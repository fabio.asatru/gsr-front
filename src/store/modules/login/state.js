const state = {
  login : {
    username: '',
    email: '',
    token: localStorage.getItem('token'),
    refreshToken: null,
    userId: 0
  }
}
