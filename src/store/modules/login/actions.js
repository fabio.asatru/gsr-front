/*
export function someAction (context) {
}
*/

// import { axiosInstance } from './../../../boot/axios'
const serverIP = 'http://192.168.1.5:8080'

export const actions = {
    logar({commit}, payload) {
        const url = serverIP + '/authenticate'
        // const url = '/authenticate'
        return this.$http.get(url, payload)
        // return axiosInstance.get(url, payload)
    },
    registrar({}, payload) {
        const url = serverIP + '/register'
        // const url = '/register'
        // return this.$http.post(url, payload)
        return this.axios.post(url, payload)
    }
}