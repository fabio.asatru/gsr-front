/*
export function someMutation (state) {
}
*/
export const mutations = {
    setToken(state, token) {
        state.token = token
        localStorage.setItem('token')
    },
    setLogin(state, auth) {
        state.username      = auth.userId
        state.email         = auth.email
        state.token         = auth.token
        state.refreshToken  = auth.refreshToken
        state.userId        = auth.userId
    }
}