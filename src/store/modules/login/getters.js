/*
export function someGetter (state) {
}
*/
export const getters = {
    login: state => {
        return state.login
    } ,
    username: state => state.username,
    userId: state => state.userId,
    token: state => state.token,
    email: state => state.email
}