import Vue from 'vue'
import Vuex from 'vuex'

import {login} from './modules/login'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const store = new Vuex.Store({
    modules: {
      login
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return store
}
